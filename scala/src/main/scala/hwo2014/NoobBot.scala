package hwo2014

import org.json4s._
import org.json4s.DefaultFormats
import org.json4s.JsonDSL._
import org.json4s.native.JsonMethods._
import java.net.Socket
import java.io.{BufferedReader, InputStreamReader, OutputStreamWriter, PrintWriter}
import org.json4s.native.Serialization
import scala.annotation.tailrec

object NoobBot extends App {
  args.toList match {
    case hostName :: port :: botName :: botKey :: _ =>
      new NoobBot(hostName, Integer.parseInt(port), botName, botKey)
    case _ => println("args missing")
  }
}

class NoobBot(host: String, port: Int, botName: String, botKey: String) {
  implicit val formats = new DefaultFormats{}
  val socket = new Socket(host, port)
  val writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream, "UTF8"))
  val reader = new BufferedReader(new InputStreamReader(socket.getInputStream, "UTF8"))
  send(MsgWrapper("join", Join(botName, botKey)))
  play

  @tailrec private def play {
    val line = reader.readLine()
    if (line != null) {
      Serialization.read[MsgWrapper](line) match {
        case MsgWrapper("yourCar", data)  => handleYourCar(data)
        case MsgWrapper("gameInit", data)  => handleGameInit(data)
        case MsgWrapper("carPositions", _) =>
        case MsgWrapper(msgType, _) =>
          println("Received: " + msgType)
      }

      send(MsgWrapper("throttle", 0.6))
      play
    }
  }

  private def handleYourCar(data: JValue) {
    val myCar = DataModel.getCarId(data)

    println("myCar " + myCar)
  }

  private def handleGameInit(data: JValue) {
    // There has got to be a better way than this with json4s...
    // ... but I'm wasting time on JSON munging.
    // TODO research json4s properly.

    // TODO extract track. id, name, pieces, lanes, starting point
    // TODO extract cars
    // val carsData = data \\ "race" \\ "cars"
    // val carIdsDimensions = for {
    //   JArray("car", JObject(car)) <- carsData
    //   JField("id", JObject(id)) <- car
    //   JField("dimensions", JObject(dimensions)) <- car
    // } yield (id, dimensions)
    // println(carIdsDimensions)

    // // extract race session
    // val raceSession = data \\ "race" \\ "raceSession"
    // val raceSessionLaps: Int          = compact(render(raceSession \\ "laps")).toInt
    // val raceSessionMaxLapTimeMs: Int  = compact(render(raceSession \\ "maxLapTimeMs")).toInt
    // val raceSessionQuickRace: Boolean = compact(render(raceSession \\ "quickRace")).toBoolean

    // val session = RaceSession(raceSessionLaps, raceSessionMaxLapTimeMs, raceSessionQuickRace)

    // println(session)
  }

  def send(msg: MsgWrapper) {
    writer.println(Serialization.write(msg))
    writer.flush
  }

}

case class Join(name: String, key: String)
case class MsgWrapper(msgType: String, data: JValue) {
}
object MsgWrapper {
  implicit val formats = new DefaultFormats{}

  def apply(msgType: String, data: Any): MsgWrapper = {
    MsgWrapper(msgType, Extraction.decompose(data))
  }
}
