package noobbot;

import java.util.*;

public class RaceContext {
    // TODO Handle quali and race - two sequences of messages.
    private JoinEvent     joinEvent;
    private YourCarEvent  yourCarEvent;
    private GameInitEvent gameInitEvent;

    public void join(JoinEvent e) {
        this.joinEvent = e;
    }

    public void yourCar(YourCarEvent e) {
        this.yourCarEvent = e;
    }

    public void gameInit(GameInitEvent e) {
        // We've got so much information in this event.
        // For now, we shall store the original info. Analysis will follow later.
        this.gameInitEvent = e;

        // TODO Should now calculate the length and curvature of every drivable section in the track.
        // TODO Should sum together like pieces to get e.g. length of longest straight.
        // TODO Should calculate shortest path round the track.
        // TODO Should calculate max safe cornering speed everywhere - may need more data
    }

    public void gameStart() {
        System.out.println("***** GAME HAS STARTED *****");
    }
}
