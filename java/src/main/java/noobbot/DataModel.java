package noobbot;

import java.util.*;

public class DataModel {
	public static <T extends Event> T create(Class<T> clazz, MsgWrapper msg) {
		T result = null;

		// Yikes. I should have done this in JavaScript...

		if (msg.msgType.equals("join")) {
			Map<String, String> dataMap = (Map<String, String>)msg.data;
			result = (T)new JoinEvent(dataMap.get("name"), dataMap.get("key"));
		}
		else if (msg.msgType.equals("yourCar")) {
			Map<String, String> dataMap = (Map<String, String>)msg.data;
			result = (T)new YourCarEvent(dataMap.get("name"), dataMap.get("color"));
		}
		else if (msg.msgType.equals("gameInit")) {
			final Map<String, Map<String, ?>> dataMap  = (Map<String, Map<String, ?>>)msg.data;

			final Map<String, ?> raceMap = (Map<String, ?>)dataMap.get("race");
			final Map<String, ?> trackMap = (Map<String, ?>)raceMap.get("track");

			final String trackId   = (String)trackMap.get("id");
			final String trackName = (String)trackMap.get("name");

			final List<Map<String, ?>> piecesArray = (List<Map<String, ?>>)trackMap.get("pieces");
			List<TrackPiece> trackPieces = new ArrayList<TrackPiece>();
			for (Map<String, ?> pieceJson : piecesArray) {
				// These are all optional.
				final Double  length   = (Double)  pieceJson.get("length");
				final Boolean isSwitch = (Boolean) pieceJson.get("switch");
				final Double radius    = (Double)  pieceJson.get("radius");
				final Double angle     = (Double)  pieceJson.get("angle");

				if (radius != null) {
					// Curved piece
					trackPieces.add(new CurvedPiece(isSwitch == null ? false : isSwitch,
						                            radius, angle));
				}
				else {
					trackPieces.add(new StraightPiece(isSwitch == null ? false : isSwitch, length));
				}
			}

			final List<Map<String, Double>> lanesArray  = (List<Map<String, Double>>)trackMap.get("lanes");
			List<Lane> lanes = new ArrayList<Lane>();
			for (Map<String, Double> laneJson : lanesArray) {
				final Double distanceFromCenter = (Double) laneJson.get("distanceFromCenter");
				final Double index              = (Double) laneJson.get("index");
				lanes.add(new Lane(distanceFromCenter, index));
			}

			final Map<String, ?> startingPointMap = (Map<String, ?>)trackMap.get("startingPoint");
			final Map<String, Double> position = (Map<String, Double>)startingPointMap.get("position");
			final Double angle = (Double) startingPointMap.get("angle");
			final Double x     = (Double) position.get("x");
			final Double y     = (Double) position.get("y");
			final StartingPoint startingPoint = new StartingPoint(x, y, angle);

			final List<Map<String, ?>> carsArray = (List<Map<String, ?>>)raceMap.get("cars");
			List<Car> cars = new ArrayList<Car>();
			for (Map<String, ?> carJson : carsArray) {
				final Map<String, String> carId = (Map<String, String>)carJson.get("id");
				final Map<String, Double> carDimensions = (Map<String, Double>)carJson.get("dimensions");
				final String carName  = (String) carId.get("name");
				final String carColor = (String) carId.get("color");
				final Double carLength = (Double) carDimensions.get("length");
				final Double carWidth  = (Double) carDimensions.get("width");
				final Double carGuideFlagPosition = (Double) carDimensions.get("guideFlagPosition");

				cars.add(new Car(carName, carColor, carLength, carWidth, carGuideFlagPosition));
			}

			final Map<String, ?> sessionMap = (Map<String, ?>)raceMap.get("raceSession");
			final double laps         = (Double)sessionMap.get("laps");
			final double maxLapTimeMs = (Double)sessionMap.get("maxLapTimeMs");
			final boolean quickRace   = (Boolean)sessionMap.get("quickRace");
			final RaceSession raceSession = new RaceSession(laps, maxLapTimeMs, quickRace);

			result = (T)new GameInitEvent(trackId, trackName, trackPieces, lanes,
					   				      startingPoint, cars, raceSession);
		}
		else {
			throw new IllegalArgumentException("Unrecognised event type: " + msg.msgType);
		}

		return result;
	}
}

class Event {
}

class JoinEvent extends Event {
	public final String name;
	public final String key;

	public JoinEvent(String name, String key) {
		this.name = name;
		this.key  = key;
	}
}

class YourCarEvent extends Event {
	public final CarId carId;

	public YourCarEvent(String name, String color) {
		this.carId = new CarId(name, color);
	}
}

class GameInitEvent extends Event {
	public final String           trackId;
	public final String           trackName;
	public final List<TrackPiece> pieces;
	public final List<Lane>       lanes;
	public final StartingPoint    startingPoint;
	public final List<Car>        cars;
	public final RaceSession      raceSession;

	public GameInitEvent(String trackId, String trackName, List<TrackPiece> pieces,
		                 List<Lane> lanes, StartingPoint startingPoint, List<Car> cars,
		                 RaceSession raceSession) {
		this.trackId   = trackId;
		this.trackName = trackName;
		this.pieces    = Collections.unmodifiableList(pieces);
		this.lanes     = Collections.unmodifiableList(lanes);
		this.startingPoint = startingPoint;
		this.cars          = Collections.unmodifiableList(cars);
		this.raceSession   = raceSession;
	}
}


// The actual data model - as given from the server.
class Car {
	public final CarId id;
	public final double length;
	public final double width;
	public final double guideFlagPosition;

	public Car(String name, String color, double length, double width, double guideFlagPosition) {
		this.id                = new CarId(name, color);
		this.length            = length;
		this.width             = width;
		this.guideFlagPosition = guideFlagPosition;
	}
	@Override public String toString() {
		return id.toString() + "[" + length + "," + width + "," + guideFlagPosition + "]";
	}
}

class CarId {
	public final String name;
	public final String color;
	public CarId(String name, String color) {
		this.name  = name;
		this.color = color;
	}
	@Override public String toString() {
		return "CarId(" + name + ":" + color + ")";
	}
}

abstract class TrackPiece {
	public final boolean isSwitch;
	public TrackPiece() { this(false); }
	public TrackPiece(boolean isSwitch) {
		this.isSwitch = isSwitch;
	}
}

class StraightPiece extends TrackPiece {
	// TODO This length could be computed for curved pieces - but depends on lane.
	public final double length;
	public StraightPiece(boolean isSwitch, double length) {
		super(isSwitch);
		this.length = length;
	}
	@Override public String toString() {
		return "Straight(" + (float)length + " sw:" + isSwitch + ")";
	}
}

class CurvedPiece extends TrackPiece {
	public final double radius;
	public final double angle;
	public CurvedPiece(boolean isSwitch, double radius, double angle) {
		super(isSwitch);
		this.radius = radius;
		this.angle  = angle;
	}
	@Override public String toString() {
		return "Curve(" + (float)radius + "," + (float)angle + " sw:" + isSwitch + ")";
	}
}

class Lane {
	public final double distanceFromCenter;
	public final int   index;

	public Lane(double distanceFromCenter, double index) {
		this.distanceFromCenter = distanceFromCenter;
		this.index              = (int) index;
	}
	@Override public String toString() {
		return "Lane(" + index + "," + (float)distanceFromCenter + ")";
	}
}

class StartingPoint {
	public final Position position;
	public final double    angle;
	public StartingPoint(double x, double y, double angle) {
		this.position = new Position(x, y);
		this.angle    = angle;
	}
}

class Position {
	public final double x;
	public final double y;
	public Position(double x, double y) {
		this.x = x;
		this.y = y;
	}
}

class RaceSession {
	public final int laps;
	public final double maxLapTimeMs;
	public final boolean quickRace;
	public RaceSession(double laps, double maxLapTimeMs, boolean quickRace) {
		this.laps         = (int)laps;
		this.maxLapTimeMs = maxLapTimeMs;
		this.quickRace    = quickRace;
	}
}
